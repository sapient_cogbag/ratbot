'''
Documentation, License etc.

@package ratbot
'''
import typing 
import discord
import os
import os.path
from discord import Client
from discord.ext import commands, tasks
import asyncio

data_prefix = "./data"
command_prefix="!!r"
client_id=604022745856213054

class IsOwnerSentinel:
    def __init__(self):
        pass

def count(iterable: iter, predicate) -> int:
    """
    Count the number of things in iterable that return True when the predicate function is called on them.
    """
    return len([k for k in iterable if predicate(k)])

def id_mash(ident: int) -> str:
    return hex(ident)

def get_server_folder_name(server: discord.Guild) -> str:
    return os.path.join(data_prefix, id_mash(server.id))

def get_channel_settings_file_name(text_channel: discord.TextChannel) -> str:
    return os.path.join(get_server_folder_name(text_channel.guild()), id_mash(text_channel.id()))

def get_server_settings_allowed_admin_roles_fname(server: discord.Guild) -> str:
    """
    file contains a list of roles allowed to admin this bot.
    """
    return os.path.join(get_server_folder_name(server), "z-allowed-roles")

def get_server_settings_allowed_admin_users_fname(server: discord.Guild) -> str:
    return os.path.join(get_server_folder_name(server), "z-allowed-users")

class ServerPermissions(object):
    def __init__(self):
        self._allowed_roles: typing.Set[int] = set()
        self._allowed_users: typing.Set[int] = set()
        
    def load_roles_from_file(self, file_contents:str):
        """
        Load allowed roles into this server permission object from the contents of a file.
        Invalid lines will be ignored.
        """
        lines = file_contents.split("\n")
        for line in lines:
            try:
                self._allowed_roles.add(int(line))
            except ValueError:
                pass
            
    def load_users_from_file(self, file_contents:str):
        """
        Load specifically allowed users into this server permission object from the contents of a file.
        Invalid lines will be ignored.
        """
        lines = file_contents.split('\n')
        for line in lines:
            try:
                self._allowed_users.add(int(line))
            except ValueError:
                pass
    
    def is_user_allowed(self, member: discord.Member) -> bool:
        allowed = len(self.get_source_of_bot_power(member)) > 0
        return allowed
    
    def is_role_allowed(self, role: discord.Role) -> bool:
        return role.id in self._allowed_roles
    
    def allow_user(self, member: discord.Member):
        self._allowed_users.add(member.id)
        
    def allow_role(self, role: discord.Role):
        self._allowed_roles.add(role.id)
        
    def disallow_user(self, member: discord.Member):
        self._allowed_users.remove(member.id)
        
    def disallow_role(self, role: discord.Role):
        self._allowed_roles.remove(role.id)
        
    def get_allowed_users(self, server_on: discord.Guild) -> typing.List[discord.Member]:
        """
        Get the allowed users on the server in question.
        
        Automatically removes invalid role IDs (which give None) from the returned list.
        """
        members = [server_on.get_member(i) for i in self._allowed_users]
        members = [i for i in members if i is not None]
        return members
    
    def get_allowed_roles(self, server_on: discord.Guild) -> typing.List[discord.Role]:
        """
        Get the allowed roles on the server in question.
        
        Automatically removes invalid role IDs (which give None) from the returned list.
        """
        roles = [server_on.get_role(i) for i in self._allowed_roles]
        roles = [r for r in roles if r is not None]
        return roles
    
    def get_allowed_user_ids(self) -> typing.Set[int]:
        """
        Get all users explicitly allowed to modify the bot settings.
        """
        return set([i for i in self._allowed_users])
    
    def get_allowed_role_ids(self) -> typing.Set[int]:
        """
        Get all roles allowed to modify the bot settings.
        """
        return set([i for i in self._allowed_roles])
    
    def get_role_file_contents(self) -> str:
        """
        Get the contents of the rolefile that describes the allowed roles in this server permission.
        """
        lines = [str(x) for x in self._allowed_roles]
        return "\n".join(lines)
    
    def get_user_file_contents(self) -> str:
        """
        Get the contents of the userfile that describes the explicitly allowed users in this server permission.
        """
        lines = [str(x) for x in self._allowed_users]
        return "\n".join(lines)
    
    def filter_by_validity_on_server(self, server: discord.Guild):
        """
        Filter stored role and user ids by whether they actually mean something/exist on the server.
        
        This is a good way to purge server members who have left from the allowed admin list.
        """
        self._allowed_roles = set([rid for rid in self._allowed_roles if server.get_role(rid) is not None])
        self._allowed_users = set([mid for mid in self._allowed_users if server.get_member(mid) is not None])

    def get_source_of_bot_power(self, member: discord.Member)\
            -> typing.List[typing.Union[discord.Member, discord.Role, IsOwnerSentinel]]:
        """
        Get the sources of a given member's bot admin rights (if it has any).
        
        If it does not have any rights, we return an empty list.
        If the member is the owner of the server, then an instance of IsOwnerSentinel is returned.
        If the member has one or more roles that give them bot powers, they will be included in
        this list.
        If the member is a user specifically given bot powers, they will be put into 
        """
        roles = member.roles
        allowed_roles_in_member = [r for r in roles if self.is_role_allowed(r)]
        allowed_members = [m for m in [member] if (m.id in self._allowed_users)]
        is_server_owner = (member.id == member.guild.owner.id)
        
        result = []
        result.extend(allowed_roles_in_member)
        result.extend(allowed_members)
        result.append(IsOwnerSentinel()) if is_server_owner else None
        return result
        
        
        

def conditional_empty_init_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)
        

def conditional_empty_init_file(path, binary=False):
    conditional_empty_init_dir(os.path.dirname(path))
    if not os.path.exists(path):
        if binary:
            with open(path, "wb") as f:
                f.write(b"")
        else:
            with open(path, "wt") as f:
                f.write("")
        

def load_or_init_permissions_for_server(server: discord.Guild) -> ServerPermissions:    
    role_perm_file = get_server_settings_allowed_admin_roles_fname(server)
    user_perm_file = get_server_settings_allowed_admin_users_fname(server)
    
    conditional_empty_init_file(role_perm_file, binary=False)
    conditional_empty_init_file(user_perm_file, binary=False)
            
    perms = ServerPermissions()
    with open(role_perm_file, "rt") as f:
        perms.load_roles_from_file(f.read())
    with open(user_perm_file, "rt") as f:
        perms.load_users_from_file(f.read())
        
    return perms

def flush_permissions_for_server(server: discord.Guild, perms: ServerPermissions):
    role_perm_file = get_server_settings_allowed_admin_roles_fname(server)
    user_perm_file = get_server_settings_allowed_admin_users_fname(server)
    
    conditional_empty_init_file(role_perm_file, binary=False)
    conditional_empty_init_file(user_perm_file, binary=False)
    
    with open(role_perm_file, "wt") as f:
        f.write(perms.get_role_file_contents())
    with open(user_perm_file, "wt") as f:
        f.write(perms.get_user_file_contents())
        
def wipe_permissions_for_server(server: discord.Guild):
    """
    Erase the current permission settings for a server. This will ensure that only the owner can use the bot.
    
    Note that this requires reloading from disk afterwards.
    """
    flush_permissions_for_server(server, ServerPermissions())
    
    
def identifier_message_embed(entity: typing.Union[discord.Member, discord.Role]) -> str:
    """
    Creates a single-line message for a member or role that identifies them by name for usage in an embed.
    """
    return entity.mention
    
    
async def group_no_cmd(context: commands.Context):
    """
    Function to call when a group command requiring subcommands does not get one.
    """
    await context.send_help(context.command) # Help for the invoked command.    

def gen_user_and_role_list(
        *members_and_roles: typing.List[typing.Union[discord.Member, discord.Role]], **kwargs) -> str:
    """
    Generate a multiline description listing users and roles in the list. Splits them into "Users:" and "Roles:"
    section. If there are none of one type or the other then don't insert that section.
    
    Passing the keyword argument "usertitle" will change the title of the user section if it exists.
    (it's auto-converted to a string)
    Passing the keyword argument "roletitle" will change the title of the role section if it exists.
    (it's auto-converted to a string)
    """
    users: typing.List[discord.Member] = []
    roles: typing.List[discord.Role] = []
    
    usertitle = str(kwargs.get("usertitle", "**Users:**"))
    roletitle = str(kwargs.get("roletitle", "**Roles:**"))
    
    for entity in members_and_roles:
        if isinstance(entity, discord.Member):
            users.append(entity)
        elif isinstance(entity, discord.Role):
            roles.append(entity)
            
    # Generate description.
    message_users = "\n".join([identifier_message_embed(x) for x in users])
    message_roles = "\n".join([identifier_message_embed(x) for x in roles])
    desc_users = "\n" + usertitle + "\n" + message_users
    desc_roles = "\n" + roletitle + "\n" + message_roles
    desc = ""
    if len(roles) != 0:
        desc += desc_roles
    if len(users) != 0:
        desc += desc_users
    return desc.strip()  # Remove any leading or trailing newlines.

    
class Perms(commands.Cog):
    """
    Permissions controller for the command.
    """
    def __init__(self, bot: commands.Bot):
        self.bot: commands.bot.Bot = bot
        # Mapping from server ids to permissions data.
        self._current_permissions: typing.Dict[int, ServerPermissions] = {}
        if bot.is_ready():
            self.load_info_from_disk_sync()
            self.purge_all_invalid_permissions()
        
    @commands.Cog.listener(name="on_member_remove")
    @commands.Cog.listener(name="on_member_join")
    async def clear_member_perms(self, member: discord.Member):
        """
        Clear the perms for a member.
        
        If a member joins then they left at some point so their perms should be cleared.
        """
        perms = self.get_perms(member.guild)
        perms.disallow_user(member)
        self.flush_server_perms(member.guild)
        
    @commands.Cog.listener(name="on_guild_role_create")
    @commands.Cog.listener(name="on_guild_role_remove")
    async def clear_role_perms(self, role: discord.Role):
        """
        New, deleted, or previously deleted roles should have no perms.
        """
        perms = self.get_perms(role.guild)
        perms.disallow_role(role)
        self.flush_server_perms(role.guild)
        
    @commands.Cog.listener(name="on_guild_remove")
    @commands.Cog.listener(name="on_guild_join")
    async def server_reset_permissions(self, server: discord.Guild):
        self._current_permissions[server.id] = ServerPermissions()
        self.flush_server_perms(server)
    
    def purge_invalid_permission_data(self, server: discord.Guild):
        """
        Removes permission data for a member or user (e.g. when they leave).
        
        This scans the whole server for people who are no longer there.
        """
        self.get_perms(server).filter_by_validity_on_server(server)
        self.flush_server_perms(server)
        
    def purge_all_invalid_permissions(self):
        """
        Purge all of the invalid permission data for all the guilds, and flush to disk.
        
        Expensive.
        """
        for server in self.bot.guilds:
            self.purge_invalid_permission_data(server)
    
    @commands.Cog.listener(name="on_ready")
    async def load_info_from_disk(self):
        self.load_info_from_disk_sync()
        
    def load_info_from_disk_sync(self):
        for server in self.bot.guilds:
            self._current_permissions[server.id] = load_or_init_permissions_for_server(server)
            
    # Check permissions.
    async def bot_check(self, ctx: commands.Context):
        usr = ctx.author 
        user_permissions: discord.Permissions = usr.permissions_in(ctx.channel)
        if user_permissions.administrator and self.has_bot_permissions(usr):
            return True
        elif not user_permissions.administrator:
            p = discord.Permissions.none()
            p.administrator(True)
            raise commands.MissingPermissions([p])
        else:
            raise commands.MissingPermissions(["botpermissions"])
    
    def get_perms(self, server: discord.Guild) -> ServerPermissions:
        """Get the permissions for a server, initialising it if it does not already exist."""
        return self._current_permissions.setdefault(server.id, ServerPermissions())
    
    def flush_server_perms(self, server: discord.Guild):
        """Flush the permissions for the given server"""
        perms: ServerPermissions = self.get_perms(server)
        flush_permissions_for_server(server, perms)
        
    async def cog_after_invoke(self, ctx: commands.Context):
        # Flush the data for the given server to disk.
        self.flush_server_perms(ctx.guild)
    
    @commands.group(invoke_without_command=True)
    async def admin(self, context: commands.Context):
        """
        Command group for administrative tasks.
        
        To use this command group you need admin permissions on the server and also
        have permissions with the bot, or be the server owner.
        """
        await group_no_cmd(context)
        
    @admin.command()
    async def add(self, 
                  context: commands.Context, 
                  *members_and_roles: commands.Greedy[typing.Union[discord.Member, discord.Role, typing.Any]]
        ):
        """
        Allow users or roles to perform admin tasks on the bot.
        """
        if len(members_and_roles) == 0:
            await context.send_help(self.add)
            return

        for role_or_member in members_and_roles:
            if isinstance(role_or_member, discord.Member):
                self._current_permissions[context.guild.id].allow_user(role_or_member)
            elif isinstance(role_or_member, discord.Role):
                self._current_permissions[context.guild.id].allow_role(role_or_member)
        
        desc = gen_user_and_role_list(*members_and_roles)
        await context.send(embed=discord.Embed(
            title="Ratbot Administrative Powers Added", 
            description=desc,
            colour=discord.colour.Color.green()
        ))
        
    @admin.command()
    async def remove(self,
                     context: commands.Context,
                     *members_and_roles: commands.Greedy[typing.Union[discord.Member, discord.Role]]
        ):
        """
        Unallow users or roles to perform admin tasks on the bot.
        
        Note that if a disallowed user has a role that is allowed and Administrator discord permissions
        they will still be able to perform administrative tasks on the bot and if they just have
        an allowed role they can still perform general bot tasks.
        """
        if len(members_and_roles) == 0:
            await context.send_help(self.remove)
            return
        
        server: discord.Guild = context.guild
        for role_or_member in members_and_roles:
            if isinstance(role_or_member, discord.Member):
                self.get_perms(server).disallow_user(role_or_member)
            elif isinstance(role_or_member, discord.Role):
                self.get_perms(server).disallow_role(role_or_member)

        desc = gen_user_and_role_list(*members_and_roles)
        await context.send(embed=discord.Embed(
            title="Ratbot Administrative Powers Removed", 
            description=desc,
            colour=discord.colour.Color.red()
        ))
        
    def has_bot_permissions(self, member: discord.Member) -> bool:
        """
        Return whether the member has permissions to do general stuff.
        
        If the server doesn't yet have permissions then simply create a new one in the dictionary.
        It will automatically be flushed to disk.
        """
        perms = self.get_perms(member.guild)
        return perms.is_user_allowed(member)
    
    @admin.command()
    async def invite(self, ctx: commands.Context):
        """
        Get an invite link for the bot.
        """
        permissions = discord.Permissions()
        permissions.manage_messages = True
        permissions.read_message_history = True
        permissions.read_messages = True
        permissions.add_reactions = True
        permissions.send_messages = True
        permissions.embed_links = True
        permissions.manage_channels = True
        await ctx.send(discord.utils.oauth_url(client_id, permissions=permissions))
        
    @admin.command()
    async def show(self, ctx: commands.Context):
        """
        Show the users and roles with ratbot permissions.
        """
        roles = self.get_perms(ctx.guild).get_allowed_roles(ctx.guild)
        users = self.get_perms(ctx.guild).get_allowed_users(ctx.guild)
        await ctx.send(embed=discord.Embed(
            title="Has Bot Permissions",
            description=gen_user_and_role_list(*roles, *users),
            colour=discord.colour.Color.blue()
        ))
    
    @admin.command()
    async def check(self, ctx: commands.Context, member_or_role: typing.Union[discord.Member, discord.Role]):
        """
        Check if a user or role has bot permissions (the user check includes any roles they have), and report
        why they have them.
        """
        has_power = False
        server = member_or_role.guild
        perms: ServerPermissions = self.get_perms(server)
        
        if isinstance(member_or_role, discord.Member):
            power_source_list = perms.get_source_of_bot_power(member_or_role)
            # Break out if the user does not have admin perms
            # We use name rather than mention because mentions/referrals break in embed titles.
            if len(power_source_list) == 0:
                await ctx.send(embed=discord.Embed(
                    title="User @" + member_or_role.name + " has no bot permissions",
                    description = "No, really, they don't. I promise.",
                    colour=discord.colour.Colour.red()
                ))
                return
            
            is_server_owner = (count(power_source_list, lambda x: isinstance(x, IsOwnerSentinel)) > 0)
            users = [x for x in power_source_list if isinstance(x, discord.Member)]  # Should be just the member
            # but easier if we do this just in case.
            roles = [x for x in power_source_list if isinstance(x, discord.Role)]
            
            desc = gen_user_and_role_list(
                *users, *roles,
                usertitle="**Explicit User Permissions Granted To:**",
                roletitle="**User Has Roles:**"
            )
            if is_server_owner:
                desc += "\n" + "**User is server owner.**"
            await ctx.send(embed=discord.Embed(
                title="Sources of User " + member_or_role.name + "'s Bot Admin Powers",
                description=desc,
                colour=discord.colour.Color.green()
            ))
            
        elif isinstance(member_or_role, discord.Role):
            has_power = perms.is_role_allowed(member_or_role)
            if has_power:
                await ctx.send(embed=discord.Embed(
                    title="Role " + member_or_role.name + " has bot admin powers.",
                    description="",
                    colour=discord.colour.Colour.green()
                ))
            else:
                await ctx.send(embed=discord.Embed(
                    title="Role " + member_or_role.name + " has no bot admin powers.",
                    description="I promise ;-)",
                    colour=discord.colour.Colour.red()
                ))
                
    @commands.command(name="~qr")
    async def emergency_remove(
        self, 
        ctx: commands.Context, 
        *args: commands.Greedy[discord.Member]
    ):
        """
        Command that immediately strips individuals of their bot admin rights, even if that requires removing
        admin rights from their roles. Dangerous but good for emergency situations. 
        
        Naturally this does not work on the server owner. It will remove all the bot admin rights of any roles they
        have but it won't stop them from being the server dictator.
        """
        if len(args) == 0:
            await ctx.send_help(self.emergency_remove)
            return
        
        sources_of_power = set()
        perms = self.get_perms(ctx.guild)
        for member_or_role in args:
            user_power_sources = self.get_perms(ctx.guild).get_source_of_bot_power(member_or_role)
            for i in user_power_sources:
                sources_of_power.add(i)
        
        # Actually remove the power:
        for source in sources_of_power:
            # Ignore IsOwnerSentinel because there is no way to remove their power.
            if isinstance(source, discord.Role):
                perms.disallow_role(source)
            elif isinstance(source, discord.Member):
                perms.disallow_user(source)
        
        output_description=""
        output_description += gen_user_and_role_list(*args, usertitle="**Stripped all power I can from:**")
        output_description += "\n------------\n"
        output_description += gen_user_and_role_list(
            *sources_of_power, 
            usertitle="**Removed Bot Power From Users:**",
            roletitle="**Removed Bot Power From Roles:**"
        )
        
        await ctx.send(embed=discord.Embed(
            title="Done",
            description=output_description,
            colour=discord.colour.Colour.red()
        ))
        
        
def is_in_servers(*guild_ids):
    async def predicate(ctx):
        return ctx.guild and (ctx.guild.id in guild_ids)
    return commands.check(predicate)

discord_bot_testing = 315508753229283328

# Discord-Bot-Testing
# Personal eval command
# https://discordpy.readthedocs.io/en/latest/ext/commands/commands.html
# @bot.command(name='eval')
# @commands.is_owner()
# @is_in_servers(discord_bot_testing)
# async def _eval(ctx, *, code):
    # """A bad example of an eval command"""
    # await ctx.send(eval(code))
    

def main():
    # Space required here to ensure !!ratbot has a space afterwards
    bot = commands.Bot(command_prefix = command_prefix + " ")
    token = None
    with open("./token", "rt") as token_file:
        token = token_file.readline()
    token = token.strip()
    bot.add_cog(Perms(bot))
    bot.run(token)

if __name__ == "__main__":
    main()
